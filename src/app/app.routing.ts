import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule  } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { IconsComponent } from './icons/icons.component';
import { MapsComponent } from './maps/maps.component';
import { TableListComponent } from './table-list/table-list.component';
import { LoginComponent } from './login/login.component';
const routes: Routes =[
  {
    path: '',
    redirectTo: 'maps',
    pathMatch: 'full',
  }, {
    path: '',
    component: AdminLayoutComponent,
    children: [{
      path: '',
      loadChildren: './layouts/admin-layout/admin-layout.module#AdminLayoutModule'
    }]
  }, {
    path: 'details',
    component: IconsComponent,
    children: [{
      path: '',
      loadChildren: './layouts/admin-layout/admin-layout.module#AdminLayoutModule'
    }]
  }, {
    path: 'table-list',
    component: TableListComponent,
    children: [{
      path: '',
      loadChildren: './table-list/table-list.module#TableListModule'
    }]
  },{
    path: 'maps',
    component: MapsComponent,
    children: [{
      path: '',
      loadChildren: './maps/maps.module#MapsModule'
    }]
  },{
    path: 'login',
    component: LoginComponent,
    
  }
];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes,{
       useHash: true
    })
  ],
  exports: [
  ],
})
export class AppRoutingModule { }
