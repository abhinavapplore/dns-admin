import { Component , OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorageService } from 'angular-2-local-storage';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  user;
  constructor(public router:Router , public localStorageService:LocalStorageService){

  }
 
  ngOnInit(){
    this.user =   this.localStorageService.get('user');
    this.user = JSON.parse(this.user);
    console.log(this.user);
    if(this.user){
      this.router.navigateByUrl('table-list');
    }else{
      this.router.navigateByUrl('login');
    }
  }
  
}
