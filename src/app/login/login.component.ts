import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';
import { Router } from '@angular/router';
import { LocalStorageService } from 'angular-2-local-storage';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  otpSent: boolean = false
  phoneNumber = null
  otp = null
  confirmationResult;
  recaptchaVerifier;

  constructor(public router:Router, public localStorageService:LocalStorageService) { }
  ngOnInit() {
    this.recaptchaVerifier = new firebase.auth.RecaptchaVerifier("sign-in-button", {
      'size': 'invisible'
      });
      
  }
 
  
  sendOtp() {
    firebase.auth().signInWithPhoneNumber('+91' + this.phoneNumber, this.recaptchaVerifier)
    .then((confirmationResult) => {
    // SMS sent. Prompt user to type the code from the message, then sign the
    // user in with confirmationResult.confirm(code).
    this.confirmationResult = confirmationResult;
    console.log(this.confirmationResult);
    this.otpSent = true;
    }).catch(err => {
    console.log(err)
    })
    }


    signIn() {
      console.log(this.otp)
      this.otp=this.otp.toString();
      this.confirmationResult.confirm(this.otp).then(user=>{
      console.log(user);
      console.log("hey");
      this.localStorageService.set('user',JSON.stringify(user));
      this.router.navigateByUrl('table-list');
      })
      }

}
