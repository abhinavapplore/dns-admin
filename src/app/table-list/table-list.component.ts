import { Component, OnInit } from '@angular/core';


import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { isGeneratedFile } from '@angular/compiler/src/aot/util';
import { Router } from '@angular/router';

import { LocalStorageService } from 'angular-2-local-storage';

@Component({
  selector: 'app-table-list',
  templateUrl: './table-list.component.html',
  styleUrls: ['./table-list.component.css']
})
export class TableListComponent implements OnInit {
  // public books: FirebaseListObservable< any[] >;
  // items: AngularFireList<any[]>;
  items:any=[];
  forms:any=[];
  data:boolean=false;
  constructor(public af: AngularFireDatabase, public router:Router,public localStorageService:LocalStorageService) {
   
   
    this.items = af.list('/userReq', ref => ref);
    this.items.snapshotChanges().subscribe((res) => {
      this.items = res;
      console.log(this.items);
      // res.map(change => ({key: change.payload.key, ...change.payload.val()}));
   });
    
  }

  ngOnInit() {
  
  }

  showMore(item, index){
    console.log(item);
    console.log(index);
    
    this.data=true;

  }

  showLess(item, index){
    console.log(item);
    console.log(index);
    this.data=true;
  }

  selectUser(item){
    var myArray=[];
 this.forms=[];
    const allUserForms = this.af.database.ref('/userReq/'+item.key);
    var self=this;
    allUserForms.orderByValue().on("value", function(snapshot) {
        console.log(snapshot.val());
       
      var resp = snapshot.val(); 
      
        console.log(resp);
      //   const objOfObjs = {
      //     "one": {"id": 3},
      //     "two": {"id": 4},
      //  };
       
      //  const arrayOfObj = Object.entries(resp).map(e => e[1]);
      const arrayOfObj = Object.keys(resp).map(i => resp[i])
       console.log(arrayOfObj);
      //  this.forms=arrayOfObj;
    
for(let i=0;i<arrayOfObj.length;i++){
  var finalOBJs=[];

   finalOBJs = Object.entries(arrayOfObj[i]).map(e => e[1]);

  myArray.push(finalOBJs[0]);
  
  
}
// console.log(myArray);
      self.forms=myArray;
       
        // snapshot.forEach(function(data) {
        //     console.log(data.key);
        // });
    }); 
    console.log("below");
    console.log(this.forms);
  }


  details(item){
    console.log("HIT");
    console.log(item);
    // localStorage.setItem('showItem',JSON.stringify(item));
    this.localStorageService.set('showItem',JSON.stringify(item));
    this.router.navigateByUrl('details');
  }

  

}
