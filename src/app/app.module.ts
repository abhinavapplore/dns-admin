import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';


import { AppRoutingModule } from './app.routing';
import { ComponentsModule } from './components/components.module';

import { AppComponent } from './app.component';

import { DashboardComponent } from './dashboard/dashboard.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { TableListComponent } from './table-list/table-list.component';
import { TypographyComponent } from './typography/typography.component';
import { IconsComponent } from './icons/icons.component';
import { MapsComponent } from './maps/maps.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { UpgradeComponent } from './upgrade/upgrade.component';
import {
  AgmCoreModule
} from '@agm/core';
import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabase } from 'angularfire2/database';
import { environment } from '../environments/environment';
import * as firebase from 'firebase';
import { LocalStorageModule } from 'angular-2-local-storage';
import { LoginComponent } from './login/login.component';



const firebaseConfig = {
  apiKey: "AIzaSyD-hsUDFUMeZDiJJ-G_QRkjBQunrKLJzBA",
  authDomain: "deathnotification-7c472.firebaseapp.com",
  databaseURL: "https://deathnotification-7c472.firebaseio.com",
  projectId: "deathnotification-7c472",
  storageBucket: "deathnotification-7c472.appspot.com",
  messagingSenderId: "789223671109",
  appId: "1:789223671109:web:749178d97d6b130d146a2c"
};
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
@NgModule({
  imports: [
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    ComponentsModule,
    RouterModule,
    AppRoutingModule,LocalStorageModule,
    AngularFireModule.initializeApp(environment.firebase),
    LocalStorageModule.forRoot({
      prefix: 'my-app',
      storageType: 'localStorage'
  })
  ],
  declarations: [
    AppComponent,
    AdminLayoutComponent,
    LoginComponent,

  ],
  providers: [AngularFireDatabase],
  bootstrap: [AppComponent]
})
export class AppModule { }
