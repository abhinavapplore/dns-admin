import { Component, OnInit } from '@angular/core';
import { LocalStorageService } from 'angular-2-local-storage';

@Component({
  selector: 'app-icons',
  templateUrl: './icons.component.html',
  styleUrls: ['./icons.component.css']
})
export class IconsComponent implements OnInit {
  details:any={};
  constructor(public localStorageService:LocalStorageService) { 
    this.details =   this.localStorageService.get('showItem');
   this.details = JSON.parse(this.details);
   console.log(this.details);
  }

  ngOnInit() {
  }

}
