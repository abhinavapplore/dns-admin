// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyD-hsUDFUMeZDiJJ-G_QRkjBQunrKLJzBA",
    authDomain: "deathnotification-7c472.firebaseapp.com",
    databaseURL: "https://deathnotification-7c472.firebaseio.com",
    projectId: "deathnotification-7c472",
    storageBucket: "deathnotification-7c472.appspot.com",
    messagingSenderId: "789223671109",
    appId: "1:789223671109:web:749178d97d6b130d146a2c"
  }
};
