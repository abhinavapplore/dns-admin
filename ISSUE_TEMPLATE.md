<!--
 IMPORTANT: Please use the following link to create a new issue:

  https://www.applore.com/new-issue/material-dashboard-angular2

**If your issue was not created using the app above, it will be closed immediately.**
-->

<!--
Love Applore? Do you need Angular, React, Vuejs or HTML? You can visit:
👉  https://www.applore.com/bundles
👉  https://www.applore.in/
-->
